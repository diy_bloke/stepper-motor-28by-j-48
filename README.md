# Stepper motor 28BY J-48 Flange

#contains a flange for the 28BY J-48 stepper motor

#contains a case + lid for the ULN2003 driver board

#contains a bracket for the 28BY J-48 motor


20210731_183932.jpg Photo of the ULN driverboard casing

28BYJ-48.png 	Photo of bracket	

motor.png 	    picture of the flange

stepper.amf 	amf file of bracket

stepper.stl 	stl file of bracket

stepper2.amf 	amd file of flange

uln.amf 	casing for ULN driver board

uln_deksel.amf 	lid for said casing
